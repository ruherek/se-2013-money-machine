# written by: Rylan Uherek, Mandeep Desai
# tested by: Rylan Uherek, Mandeep Desai
# debugged by: Rylan Uherek, Avinash Oza, Mozam Todiwala

from django.conf.urls import patterns, include, url
from django.contrib.staticfiles.urls import staticfiles_urlpatterns

urlpatterns = patterns('',
	url(r'^$', 'portfolio.views.home', name='home'),
	url(r'^(?P<leagueuser_id>\d+)/trade/$', 'portfolio.views.trade', name='trade'),
	url(r'^(?P<leagueuser_id>\d+)/detail/$', 'portfolio.views.detail', name= 'detail'),
)

urlpatterns += staticfiles_urlpatterns()

