#!/usr/bin/python

# Broker API
# Written by Rylan Uherek

"""

NOTES:
API Makes calls to Yahoo Finance API for Stock Quotes
reference sheet: http://greenido.wordpress.com/2009/12/22/yahoo-finance-hidden-api/

USAGE:
import  broker
print broker.get_data('AAPL', 'p')

"""

import urllib

# s_get_data(symbol, stat)
# symbol -- ticker symbol (e.g. 'AAPL')
# stat -- statistic on ticker (see the yahoo finance API)
# RET: the data requested by the stat

def s_get_data(symbol, stat):
	# make a call to the yahoo finance api
	url = 'http://finance.yahoo.com/d/quotes.csv?s=%s&f=%s' % (symbol, stat)
	return urllib.urlopen(url).read().strip().strip('"')

# s_get_all_data(symbol)
# symbol -- ticker symbol (e.g. 'AAPL')
# RET: all relevant data requested by the stat in a dictionary

def s_get_all_data(symbol):
	# get all of the relevant data for the stock / etf
	stock_info = s_get_data(symbol, 'l1nghopxt8').split(',')

	data = {}
	data['price'] = stock_info[0]
	data['name'] = stock_info[1]
	data['low'] = stock_info[2]
	data['high'] = stock_info[3]
	data['open'] = stock_info[4]
	data['close'] = stock_info[5]
	data['exchange'] = stock_info[6]
	data['1y-target'] = stock_info[7]

	return data
