# written by: Rylan Uherek, Avinash Oza, Mozam Todiwala, Pintu Patel, Mandeep Desai
# tested by: Rylan Uherek, Avinash Oza, Mozam Todiwala, Pintu Patel, Mandeep Desai
# debugged by: Rylan Uherek, Avinash Oza, Mozam Todiwala, Pintu Patel, Mandeep Desai

from django.conf.urls import patterns, include, url
from django.contrib.staticfiles.urls import staticfiles_urlpatterns

urlpatterns = patterns('',
	url(r'^$', 'virtualstockmarket.views.home', name='home'),
	url(r'^home$', 'virtualstockmarket.views.home', name='home'),
	url(r'^register$', 'virtualstockmarket.views.register', name='register'),
	url(r'^about_us$', 'virtualstockmarket.views.about_us', name='about_us'),
	url(r'^faq$', 'virtualstockmarket.views.faq', name='faq'),
	url(r'^login/', 'virtualstockmarket.views.login', name='login'),
        url(r'^logout/', 'virtualstockmarket.views.logout', name='logout'),
)

urlpatterns += staticfiles_urlpatterns()

