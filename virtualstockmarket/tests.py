"""
This file demonstrates writing tests using the unittest module. These will pass
when you run "manage.py test".

Replace this with more appropriate tests for your application.
"""
# written by:  Pintu Patel 
# tested by:  Pintu Patel
# debugged by: Rylan Uherek, Avinash Oza, Mozam Todiwala, Pintu Patel, Aakash Patel
from django.test import TestCase
from virtualstockmarket.models import *
from league.models import *
from django.utils import unittest
from django.test.client import Client
from datetime import datetime

class vitualstockmarketIntegrationTest(TestCase):
	fixtures = ['inital_data.json']

	def setUp(self):
		
		response = self.client.login(username='test',password='test')
		self.assertTrue(response, True )
		self.user = User.objects.get(pk=1)

	def test_index(self):
		
		response = self.client.get('/vsm/league/')
		#Test to see if the league exists
		self.assertEqual(response.context['leagues'].filter(league_name='Test League').count(),1)

		#Test to see if our trade is created in the proper place
		response = self.client.post('/vsm/portfolio/1/trade/', { 
      		"order_type": "LIMIT",  
      		"limit_price": "12.11", 
      		"order_action": "BUY", 
      		"duration": "DAY", 
      		"ticker_symbol": "F", 
      		"quantity": "176"
		} )

		self.assertEqual(response.status_code, 302 )

		#Test to see if the portfolioentry is present in the porfolio detail page for league_user id = 1
		response = self.client.get('/vsm/portfolio/1/detail/')
		self.assertEqual(response.context['portfolioentries_pending'].filter(ticker_symbol = "F", buy_entry__duration="DAY", quantity = "176", buy_entry__limit_price = "12.11").count(),1)

class vsmUnitTest1(unittest.TestCase):

		def setUp(self):
			# UnitTest1, tests to see if the league create form is has all the fields fillout with valid information, also test that the league is create after the form in submitted. If fields are left blank, or invalid, it will return back to create league form.
			self.client=Client()

		def test_page1(self):	
			response = self.client.post('/vsm/league/create',{'league_name':'test1', 'start_date' : '04/15/2013', 'end_date' : '06/14/2013', 'max_users':'20','starting_balance':'500'})
			self.assertEqual(response.status_code, 200 )		

class vsmUnitTest2(unittest.TestCase):

		def setUp(self):
			# Test to see if the League Info & Detail page is renderered for specific league id = 1, it should fail if there is no league id = 1 or if it can not display the league info page associated with the id
			self.client=Client()

		def test_page2(self):	
			response = self.client.get('/vsm/league/1/info')
			self.assertEqual(response.status_code, 200 )

class vsmUnitTest3(unittest.TestCase):

		def setUp(self):
			# Test to see if the Registration Form is correctly filled out and no fields are left blank, it should return false if any fields are left blank or if the username is not unique from the other usernames in the database.
			self.client = Client()

		def test_page3(self):

			response = self.client.post('/vsm/register', { 'FirstName' : 'user1', 'LastName':'last1','Username':'user1','Password' : 'user1','Email' : 'user1@gmail.com', 'ConfirmEmail':'user1@gmail.com' })
			
			self.assertEqual(response.status_code, 200 )

class vsmUnitTest4(unittest.TestCase):

                def setUp(self):
                        # Test to see if the league create form is has all the fields fillout with valid information, also test that the league is create after the form in submitted. It also test if a private league is created. If fields are left blank, or invalid, it will return back to create league form.
                        self.client=Client()

                def test_page4(self):   
                        response = self.client.post('/vsm/league/create',{'league_name':'test1', 'private' : '1','start_date' : '04/15/2013', 'end_date' : '06/14/2013','starting_balance':'5000','league_password':'test','league_admin':'test'})
                        self.assertEqual(response.status_code, 200 )  

class vsmUnitTest5(unittest.TestCase):

                def setUp(self):
                        # Test to see if the League Ranking page is renderered for specific league id = 1, it should fail if there is no league id = 1 or if it can not display the league info page associated with the id.
                        self.client=Client()

                def test_page5(self):   
                        response = self.client.get('/vsm/league/1/ranking')
                        self.assertEqual(response.status_code, 301)  

class vsmUnitTest6(unittest.TestCase):

                def setUp(self):
                        # Test to see if the watch list is updated with Google information in the watchlist. 
                        self.client=Client()

                def test_page6(self):   
                        response = self.client.post('/vsm/portfolio/1/detail',{'league_user':'Test League','ticker_symbol':'GOOG'})
                        self.assertEqual(response.status_code, 301)
		
