# written by: Rylan Uherek
# tested by: Rylan Uherek
# debugged by: Rylan Uherek

from django.contrib.auth.models import *
from django.contrib.sites.models import *
from django.contrib import admin
from virtualstockmarket.models import *

# remove the unecessary models
admin.site.unregister(Group)
admin.site.unregister(Site)

admin.site.register(League)
admin.site.register(LeagueUser)
