# written by: Rylan Uherek,Avinash Oza
# tested by: Rylan Uherek, Avinash Oza
# debugged by: Rylan Uherek, Avinash Oza, Mozam Todiwala
from django.conf.urls import patterns, include, url

# Uncomment the next two lines to enable the admin:
from django.contrib import admin
admin.autodiscover()

urlpatterns = patterns('',
    # Examples:
    # url(r'^$', 'moneymachine.views.home', name='home'),
    # url(r'^moneymachine/', include('moneymachine.foo.urls')),

    # Uncomment the admin/doc line below to enable admin documentation:
    # url(r'^admin/doc/', include('django.contrib.admindocs.urls')),

    # Uncomment the next line to enable the admin:
    # url(r'^admin/', include(admin.site.urls)),
	url(r'^$', 'django.views.generic.simple.redirect_to', {'url': '/vsm/home'}),
	url(r'^vsm/', include('virtualstockmarket.urls')),
	url(r'^vsm/portfolio/', include('portfolio.urls')),
	url(r'^vsm/league/', include('league.urls')),
	url(r'^admin/', include(admin.site.urls)),
)
