# written by: Rylan Uherek, Mozam Todiwala, Pintu Patel, Aakash Patel
# tested by: Rylan Uherek, Mozam Todiwala, Pintu Patel, Aakash Patel
# debugged by: Rylan Uherek, Avinash Oza, Mozam Todiwala, Aakash Patel
from django.conf.urls import patterns, include, url
from django.contrib.staticfiles.urls import staticfiles_urlpatterns

urlpatterns = patterns('',

	url(r'^$', 'league.views.home', name='home'),
	url(r'^create$', 'league.views.create_league', name='create_league'),
	url(r'^(?P<league_id>\d+)/info', 'league.views.league_info', name='league_info'),
	url(r'^join/', 'league.views.join', name='join'),
	url(r'^(?P<league_id>\d+)/admin', 'league.views.adminpanel', name='adminpanel'),
	url(r'^kick/', 'league.views.kick', name='kick'),
	url(r'^(?P<league_id>\d+)/ranking', 'league.views.league_ranking', name='league_ranking'),
)

urlpatterns += staticfiles_urlpatterns()

