# written by: Rylan Uherek, Mozam Todiwala, Aakash Patel
# tested by: Rylan Uherek, Mozam Todiwala, Aakash Patel
# debugged by: Rylan Uherek, Mozam Todiwala, Aakash Patel
from django.db import models
from django.forms import ModelForm, PasswordInput
from django.contrib.auth.models import User
from virtualstockmarket.models import *
from django import forms
# Create your models here.


class LeagueForm(ModelForm):
	private = forms.BooleanField(required=False,label="Private League")
		
        def clean(self):
		cleaned_data = super(LeagueForm,self).clean()
		private = cleaned_data.get("private")
		league_password = cleaned_data.get("league_password")
                   
     
                if private ==1 and not league_password:
			raise forms.ValidationError("put password man")
		return cleaned_data
           
        def __init__(self, *args, **kwargs):
		super(LeagueForm, self).__init__(*args, **kwargs)
                    
                    #self.fields['next_question'].queryset = Question.objects.order_by('question_text').filter(question_pool__id=question.question_pool.id).exclude(id=question_id)
     
         
	class Meta:
		model = League
			#fields = ('league_name','start_date','end_date','max_users','starting_balance')
		widgets = {
		'league_password': forms.PasswordInput(),
		}
		exclude = {'league_admin',}
